<?php
$URI = explode('?', $_SERVER['REQUEST_URI']);
$URI = preg_split('@/@', $URI[0], NULL, PREG_SPLIT_NO_EMPTY);

$Article = $URI[1];
?>
<!DOCTYPE html>
<html>
<head>
    <title>Routine.Framework</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/plugins/<?= $Article; ?>/<?= $Article; ?>.min.css">
    <link rel="stylesheet" type="text/css" href="/dependencies/prism.css">
    <link rel="stylesheet" href="/index.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/dependencies/jquery.min.js"></script>
    <?php require_once __DIR__ . '/fav.php' ?>
</head>
<body>

<?php require_once __DIR__ . '/templates/header.php' ?>
<main>
    <div class="Content">
        <?php
        $HTML = "./plugins/$Article/$Article.html";
        if (file_exists($HTML)) {
            require_once $HTML;
        }
        ?>
    </div>
    <?php require_once __DIR__ . '/templates/navbar.php' ?>
</main>
<?php require_once __DIR__ . '/templates/footer.php' ?>

<script src="/dependencies/precode.js"></script>
<script src="/dependencies/prism.js"></script>
<script src="/dependencies/jquery.actual.min.js"></script>
<script src="/dependencies/jquery.touchSwipe.min.js"></script>
<script src="<?= '/plugins/' . $Article . '/jquery.' . $Article . '.js' ?>"></script>
</body>
</html>