"use strict";

var Get_Files = function (Directory) {
    var File_System = require("fs"),
        Results = [];

    File_System.readdirSync(Directory).forEach(function (File) {

        var Stat = File_System.statSync(Directory + '/' + File);

        if (Stat && Stat.isDirectory()) {
            Results = Results.concat(File)
        }
    });
    return Results;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Require packages
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    minifyCss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    notify = require("gulp-notify"),
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    combineMq = require('gulp-combine-mq');

var Scss_Path = './plugins/',
    Css_Path = './plugins/';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


gulp.task('test', function () {

});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Watch changes in files
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('watch', function () {
    gulp.watch(Scss_Path + '/**/*.scss', ['scss']);
    gulp.watch('./scss/**/*.scss', ['project_scss']);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// scss
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('scss', function () {
    var Plugins = Get_Files('plugins');

    for (var Index = 0; Index < Plugins.length; Index++) {
        gulp.src(Scss_Path + Plugins[Index] + '/' + Plugins[Index] + '.scss')
            .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(combineMq({
                beautify: false
            }))
            .pipe(autoprefixer({browsers: ['> 1%', 'last 2 version', 'IE 9-11'], cascade: false}))
            .pipe(minifyCss())
            .pipe(sourcemaps.write())
            .pipe(rename(Plugins[Index] + '.min.css'))
            .pipe(gulp.dest(Css_Path + Plugins[Index]));
    }
});

gulp.task('project_scss', function () {
    gulp.src('./scss/index.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(autoprefixer({browsers: ['> 1%', 'last 2 version', 'IE 9-11'], cascade: false}))
        .pipe(minifyCss())
        .pipe(sourcemaps.write())
        .pipe(rename('index.min.css'))
        .pipe(gulp.dest('./'));
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////