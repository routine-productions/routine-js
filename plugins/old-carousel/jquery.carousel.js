/*
 * Copyright (c) 2015
 * Routine JS - Carousel
 * Version 0.1.0
 * Created 2016.02.02
 * Author Bunker Labs
 *
 *
 * 1) ���������� �������� ������ � ������� ��� ������� ��������
 * 2) �������� ��������� ���������� �� ����� �����. �������� ��������� ������ � ������ ������� � ������ ����������.
 *
 *
 *
 *
 */

(function ($) {
    $.fn.Carousel = function (Extended_Settings) {

        var Settings = {
            shiftElem: 9,
            duration: 200,
            start: 0
        };

        $.extend(Settings, Extended_Settings);


        var Data_Array = $.makeArray($('.JS-Carousel-Block .JS-Carousel-Data li'));
        $('.JS-Carousel-Block .JS-Carousel-Data').remove();

        var Duration = parseInt(Settings.duration);
        var WidthElemGlobal;
        var WidthAreaGlobal;
        var Visible;

        $(window).load(function () {
            var widthWindowLoad = $(window).width();
            WidthAreaGlobal = $('.JS-Carousel-Block').width();
            screenResize(widthWindowLoad, WidthAreaGlobal);
        });

        //$(window).resize(function () {
        //    var widthWindowResize = $(window).width();
        //    WidthAreaGlobal = $('.JS-Carousel-Block').width();
        //    screenResize(widthWindowResize, WidthAreaGlobal);
        //});

        function screenResize(windowWidth, widthArea) {
            if (windowWidth <= 1600) {
                rankingLoad(9, widthArea);
            }else{
                return false;
            }
        }

        function rankingLoad(visibleCount, Width_Area) {

            if ((Settings.start + visibleCount) > Data_Array.length) {
                Settings.end = Settings.start + visibleCount - Data_Array.length;

                $('.JS-Carousel-Area li').remove();

                $('.JS-Carousel-Block .JS-Carousel-Area').append(
                    Data_Array.slice(Settings.start, Data_Array.length).concat(Data_Array.slice(0, Settings.end))
                );

            } else if((Settings.start + visibleCount) == Data_Array.length){
                Settings.end = Settings.start + visibleCount;

                $('.JS-Carousel-Area li').remove();

                $('.JS-Carousel-Block .JS-Carousel-Area').append(
                    Data_Array.slice(Settings.start, Data_Array.length)
                );
            }else {

                $('.JS-Carousel-Area li').remove();

                Settings.end = Settings.start + visibleCount;

                $('.JS-Carousel-Block .JS-Carousel-Area').append(
                    Data_Array.slice(Settings.start, Settings.start + visibleCount)
                );
            }

            Visible = visibleCount;
            WidthElemGlobal = 100 / Width_Area * (Width_Area / visibleCount);


            $('.JS-Carousel-Area li').each(function (Index, Elem) {
                $(Elem).css({'left': WidthElemGlobal * Index + "%", 'width': WidthElemGlobal + "%"});
            });


            console.log(Settings.start);
            console.log(Settings.end);
        }


        //--------------------------------------------------------------------------------------------------------------

        function clickLeft() {
            if (Clicked_Left == false) {

                Clicked_Left = true;

                setTimeout(function () {
                    Clicked_Left = false;
                }, Duration);


                var Shift_Start;
                var Shift_End;


                if ((Settings.start + Settings.shiftElem) >= Data_Array.length) {
                    Shift_Start = (Settings.start + Settings.shiftElem) - Data_Array.length;
                } else {
                    Shift_Start = Settings.start + Settings.shiftElem;
                }

                if (( Settings.start + Visible + Settings.shiftElem) >= Data_Array.length) {
                    Shift_End = (Settings.start + Visible + Settings.shiftElem) - Data_Array.length;
                } else {
                    Shift_End = Settings.start + Visible + Settings.shiftElem;
                }


                if (Shift_End >= Settings.end) {
                    $('.JS-Carousel-Block .JS-Carousel-Area').append(
                        $(Data_Array.slice(Settings.end, Shift_End)).clone()
                    );

                } else {

                    $('.JS-Carousel-Block .JS-Carousel-Area').append(
                        $(Data_Array.slice(Settings.end, Data_Array.length).concat(Data_Array.slice(0, Shift_End))).clone()
                    );
                }

                console.log(Settings.start + ' ' + Settings.end + ' | ' + Shift_Start + ' ' + Shift_End);

                Settings.start = Shift_Start;
                Settings.end = Shift_End;

                function animateLeft() {
                    $('.JS-Carousel-Area li').each(function (Index, Elem) { //�����������

                        $(Elem).css({
                            'left': Index * WidthElemGlobal + "%",
                            'width': WidthElemGlobal + "%",
                            'transition-duration': Duration / 1000 + 's'
                        });

                    });

                    $('.JS-Carousel-Area li').each(function (Index, Elem) {  //��������
                        var Left = (parseInt($(Elem).css('left')) / WidthAreaGlobal * 100);
                        $(Elem).css({'left': Left - (WidthElemGlobal * Settings.shiftElem) + "%"});
                    });

                    $('.JS-Carousel-Area li').each(function (Index, Elem) {  // ��������
                        if (Index < Settings.shiftElem) {
                            $(Elem).delay(Duration).promise().done(function () {
                                $(Elem).remove()
                            });
                        }
                    });
                }

                animateLeft();
            }
        }

        function clickRight() {
            if (Clicked_Right == false) {

                Clicked_Right = true;

                setTimeout(function () {
                    Clicked_Right = false;
                }, Duration);

                var Shift_Start;
                var Shift_End;


                if ((Settings.start - Settings.shiftElem) <= 0) {
                    Shift_Start = Data_Array.length + (Settings.start - Settings.shiftElem);
                } else {
                    Shift_Start = Settings.start - Settings.shiftElem;
                }

                if (( Settings.start + Visible + Settings.shiftElem) <= 0) {
                    Shift_End = (Settings.start + Visible - Settings.shiftElem) + Data_Array.length;
                } else {
                    Shift_End = Settings.start + Visible - Settings.shiftElem;
                }


                if (Shift_Start < Settings.start) {
                    $('.JS-Carousel-Block .JS-Carousel-Area').prepend(
                        $(Data_Array.slice(Shift_Start, Settings.start)).clone()
                    );
                } else {

                    $('.JS-Carousel-Block .JS-Carousel-Area').prepend(
                        $(Data_Array.slice(Shift_Start).concat(Data_Array.slice(0, Settings.start))).clone()
                    );

                }

                console.log(Settings.start + ' ' + Settings.end + ' | ' + Shift_Start + ' ' + Shift_End);

                Settings.start = Shift_Start;
                Settings.end = Shift_End;

                function animateRight() {
                    var LeftStart = 0 - (WidthElemGlobal * Settings.shiftElem);
                    $('.JS-Carousel-Area li').each(function (Index, Elem) {  //�����������
                        $(Elem).css({
                            'left': LeftStart + "%",
                            'width': WidthElemGlobal + "%",
                            'transition-duration': Duration / 1000 + 's'
                        });
                        LeftStart = LeftStart + WidthElemGlobal;

                    });

                    $('.JS-Carousel-Area li').each(function (Index, Elem) {  // ��������
                        var Left = (parseInt($(Elem).css('left')) / WidthAreaGlobal * 100);
                        $(Elem).css({'left': Left + (WidthElemGlobal * Settings.shiftElem) + "%"});

                    });

                    $($('.JS-Carousel-Area li').get().reverse()).each(function (Index, Elem) { // ��������
                        if (Index < Settings.shiftElem) {
                            $(Elem).delay(Duration).promise().done(function () {
                                $(Elem).remove();
                            });
                        }
                    });
                }

                //animateRight();

            }
        }


        var Clicked_Left = false;
        $('.JS-Carousel-Arrow-Left').click(function () {
            clickLeft();

        });

        var Clicked_Right = false;
        $('.JS-Carousel-Arrow-Right').click(function () {
            //clickRight();
        });

    };


    $('.JS-Carousel').Carousel();
})(jQuery);

