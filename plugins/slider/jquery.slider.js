/*
 * Copyright (c) 2015
 * Routine JS - Slider
 * Version 0.1.0
 * Created 2016.01.13
 * Author Bunker Labs
 */

(function ($) {
    $.fn.JS_Modal = function (Extended_Settings) {
        var Object = {};
        $.extend(Object, {
            Slider: this,
            Slider_Handle: '.JS-Slider-Handle',
            Default_Value: '10%'
        }, Extended_Settings);

        this.each(function (Key, Value) {
            $(Value).find(Object.Slider_Handle).css('left', '10%');
        });

        Object.Slide = function (Event) {
            Object.Current_Target = Event.currentTarget;
            console.log(Object.Current_Target);

            var Shift = Math.round((Event.pageX - $(Object.Current_Target).offset().left) / $(Object.Current_Target).width() * 100);


            if ((Event.pageX - $(Object.Current_Target).offset().left) < $(Object.Current_Target).width() && (Event.pageX - $(Object.Current_Target).offset().left) > 0) {
                $(Event.currentTarget).find(Object.Slider_Handle).css('left', Shift + '%');

                $(Event.currentTarget).delay(1).promise().done(function () {
                    $(Event.currentTarget).addClass('Active');
                });
            }
        };


        Object.Slide_Move = function (Event) {
            if (Object.Current_Target) {
                var Shift = Math.round((Event.pageX - $(Object.Current_Target).offset().left) / $(Object.Current_Target).width() * 100);

                if ($(Object).hasClass('Active')) {
                    if ((Event.pageX - $(Object.Current_Target).offset().left) < $(Object.Current_Target).width() && (Event.pageX - $(Object.Current_Target).offset().left) > 0) {
                        $(Object.Current_Target).find(Object.Slider_Handle).css('left', Shift + '%');
                    }
                }
            }
        };

        Object.Stop_Slide = function (Event) {
            $(Object).removeClass('Active')
        };

        this.mousedown(Object.Slide.bind(Object));
        $(window).mouseup(Object.Stop_Slide.bind(Object));
        $(window).mousemove(Object.Slide_Move.bind(Object));

        return this;
    };

    $('.JS-Slider').JS_Modal();
})(jQuery);

