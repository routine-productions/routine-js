(function ($) {

    var Event_Click = false;
    //Write a name of your own classes into the next variables:
    var Nav_Button = $('.Hamburger');
    var Button_Close = 'Hamburger-Close';
    var Nav_Hidden = $('.Header-Links-Wrap');
    var Nav_Active = 'Hidden-Nav';
    //You shall Add data-atribute to your html teg accordingly to you own classes:
    var Click_Delay = $(Nav_Button).attr('data-click-delay') ? $(Nav_Button).attr('data-click-delay') : 0,
        Delay_Active = $(Nav_Hidden).attr('data-nav-active-delay') ? $(Nav_Hidden).attr('data-nav-active-delay') : 0,
        Delay_DisActive = $(Nav_Hidden).attr('data-nav-disactive-delay') ? $(Nav_Hidden).attr('data-nav-disactive-delay') : 0;

    $(Nav_Button).click(function () {

        if (Event_Click == false) {

            Event_Click = true;

            setTimeout(function () {
                Event_Click = false;
            }, Click_Delay);

            if ($(Nav_Hidden).hasClass(Nav_Active)) {
                setTimeout(function () {
                    $(Nav_Hidden).removeClass(Nav_Active);

                }, Delay_DisActive);

            } else {
                setTimeout(function () {
                    $(Nav_Hidden).addClass(Nav_Active);
                }, Delay_Active);
            }


            if ($(Nav_Button).hasClass(Button_Close)) {

                $(Nav_Button).removeClass(Button_Close);
            }
            else {
                $(Nav_Button).addClass(Button_Close);
            }


        }

        return false;


    });


    $('body').click(function (Event) {
        if ($(Nav_Button).hasClass(Button_Close)) {
            if (!$(Event.target).is(Nav_Hidden)) {
                setTimeout(function () {
                    $(Nav_Button).removeClass(Button_Close);
                    $(Nav_Hidden).removeClass(Nav_Active);
                }, Delay_DisActive);
            }


        }

    });


})(jQuery);