(function ($) {
    $.fn.Ripple_Effect = function () {


        $(document).on('mouseover', '.JS-Ripple-Effect', function (Event) {
            var $Splash = $('<div class="Splash">');

            if (!$('.Splash', this).length) {
                $Splash.appendTo($(this));
            }


            $($Splash).css({
                'left': Event.pageX - $(Event.currentTarget).offset().left,
                'right': $(Event.currentTarget).offset().left + $(Event.currentTarget).outerWidth() - Event.pageX
            });

            setTimeout(function () {
                $Splash.addClass('Animate');
            }, 20);
        });

        $(document).on('mouseleave', '.JS-Ripple-Effect', function (Event) {
            var $Splash = $('.Splash', this);
            $Splash.css({
                'left': Event.pageX - $(Event.currentTarget).offset().left,
                'right': $(Event.currentTarget).offset().left + $(Event.currentTarget).outerWidth() - Event.pageX
            });
            $Splash.removeClass('Animate');
            setTimeout(function () {
                $Splash.remove();
            }, 200);

        });
    };

    $('.JS-Ripple-Effect').Ripple_Effect();
})(jQuery);




