/*
 * Copyright (c) 2015
 * Routine JS - Scroll Top
 * Version 0.2.0 pre-Alpha
 * Create 2015.12.22
 * Author Bunker Labs
 */
$('.JS-Scroll-Top-Button').click(function () {
    var Duration = $(this).attr('data-scroll-duration');
    $('body, html').animate({
        scrollTop: 0
    }, Duration ? parseInt(Duration) : 800);
    return false;
});