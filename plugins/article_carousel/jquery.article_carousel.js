$.fn.Article_Carousel = function () {

    $(this).each(function (index, elem) {

        var Carousel_Width,
            Size_Carouse_List,
            Scroll_Left_Position = 0,
            Scroll_Item_Width,
            Scroll_List_Width,
            Height_Item,
            Max_Height = 0;

        function Article_Positions() {

            Carousel_Width = $(elem).width();
            Size_Carouse_List = $(elem).find('.Article-Item').size();
            if (Carousel_Width <= 600) {
                $(elem).find('.Article-Item').css({'width': Carousel_Width});
                $(elem).find('ul').css({'width': (Carousel_Width) * Size_Carouse_List});
            } else if (Carousel_Width <= 1024) {
                $(elem).find('.Article-Item').css({'width': ((Carousel_Width) / 2)});
                $(elem).find('ul').css({'width': (((Carousel_Width) / 2) * Size_Carouse_List)});
            } else if (Carousel_Width <= 1200) {
                $(elem).find('.Article-Item').css({'width': (Carousel_Width / 3)});
                $(elem).find('ul').css({'width': (Carousel_Width / 3) * Size_Carouse_List});
            }

            Scroll_Item_Width = $(elem).find('.Article-Item').outerWidth();
            Scroll_List_Width = $(elem).find('ul').outerWidth();

            $(elem).find('.Article-Item').each(function (index, elem) {
                Height_Item = $(elem).outerHeight();
                console.log(Height_Item);
                if (Height_Item > Max_Height) {
                    Max_Height = Height_Item;
                }
            });

            $(elem).css({'height': Max_Height});

            Max_Height = 0;
        }

        Article_Positions();

        $(window).resize(function () {
            Article_Positions();

            $('.JS-Article-Carousel-Prev').trigger('click');
        });


        $('.JS-Article-Carousel-Prev').click(function (e) {

            console.log(e.currentTarget);
            $(this).parents('.JS-Article-Carousel').find('.JS-Article-Carousel-Block').animate({
                scrollLeft: 0
            }, 500);

            Scroll_Left_Position = 0;

            console.log(Scroll_Left_Position);

            return false;
        });

        $('.JS-Article-Carousel-Next').click(function (e) {
            console.log(e.currentTarget);
            $(this).parents('.JS-Article-Carousel').find('.JS-Article-Carousel-Block').animate({
                scrollLeft: Scroll_List_Width - Carousel_Width
            }, 500);

            Scroll_Left_Position = Scroll_List_Width - Carousel_Width;

            console.log(Scroll_Left_Position);
            return false;
        });

        $(elem).swipe({
                swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                    if (direction == 'left') {
                        Object.Direction = 'left';

                        if (Scroll_Left_Position == Scroll_List_Width - Carousel_Width) {
                            return false;
                        } else {
                            $(this).find('.JS-Article-Carousel-Block').animate({
                                scrollLeft: Scroll_Item_Width + Scroll_Left_Position
                            }, 500);
                            Scroll_Left_Position = Scroll_Item_Width + Scroll_Left_Position;
                        }

                        console.log(Scroll_Left_Position);
                        return false;
                    } else if (direction == 'right') {
                        Object.Direction = 'right';

                        if (Scroll_Left_Position == 0) {
                            return false;
                        } else {
                            $(this).find('.JS-Article-Carousel-Block').animate({
                                scrollLeft: Scroll_Left_Position - Scroll_Item_Width
                            }, 500);
                            Scroll_Left_Position = Scroll_Left_Position - Scroll_Item_Width;
                        }

                        console.log(Scroll_Left_Position);
                        return false;
                    }
                    event.preventDefault();
                },
                allowPageScroll: "auto",
                excludedElements: "label, button, input, select, textarea, .noSwipe",
                threshold: 1,
                preventDefaultEvents: true,

                click: function (event) {
                    console.log('tap');
                }
            }
        );
    });
};


$('.JS-Article-Carousel').Article_Carousel();