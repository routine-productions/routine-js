/*
 * Copyright (c) 2015
 * Routine JS - Modal
 * Version 0.4.2 Alpha
 * Created 2015.12.03
 * Author Bunker Labs
 */
$.fn.JS_Modal = function (Object) {
    var Methods = {};
    Object = $.extend({
        Buttons: this,
        Box: '.JS-Modal',
        Close: '.JS-Modal-Close',
        Blackout: '.JS-Modal-Blackout',
        Auto_Hide: true,
        Callback: {
            Before_Show: function () {
            },
            After_Show: function () {
            },
            Before_Hide: function () {
            },
            After_Hide: function () {
            }
        }
    }, Object);

    Methods.Data_Params = function (Element) {
        $.each(Object, function (Index, Value) {
            if ((typeof Value == 'string' || typeof Value == 'number') && Element.attr('data-modal-' + Index.toLowerCase())) {
                Object[Index] = Element.attr('data-modal-' + Index.toLowerCase());
            }
        });
    };


    Methods.Show = function (Event) {
        if(Object.Auto_Hide){
            $(Object.Modal).removeClass('Visible');
        }

        Object.Target = $($(Event.currentTarget).attr('href'));

        Methods.Data_Params(Object.Target);

        Object.Callback.Before_Show();

        Object.Target
            .addClass('Visible')
            .delay(parseFloat(Object.Target.css('transition-duration')) * 1000)
            .promise().done(Object.Callback.After_Show);

        Object.Target.click(Methods.Hide);
    };

    Methods.Hide = function (Event) {
        if ($(Event.target).is(Object.Blackout) || $(Event.target).is(Object.Close)) {
            Object.Callback.Before_Hide();
            Object.Target
                .removeClass('Visible')
                .delay(parseFloat(Object.Target.css('transition-duration')) * 1000).promise()
                .done(Object.Callback.After_Hide);
        }
    };

    Object.Buttons.click(Methods.Show);

    return this;
};

$('.JS-Modal-Button').JS_Modal();


