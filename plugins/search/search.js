/*
 * Copyright (c) 2015
 * Routine JS - Search
 * Version 0.1.0
 * Created 2016.02.04
 * Author Bunker Labs
 */

$.fn.JS_Search = function (Extended_Settings) {
    var Object = {
        button: this,
        input : '.JS-Search-Input',
        form  : '.JS-Search-Form',
        query : '/search&phrase='
    };

    $.extend(Object, Extended_Settings);

    $(Object.button).click(function () {
        Object.current = this;

        $.each(Object, function (Index, Elem) {
            var Attr = $(Object.current).attr('data-search-' + Index);
            if (Attr) {
                Object[Index] = Attr;
            }
        });

        var Phrase = $(this).parents(Object.form).find(Object.input).val();
        if (Phrase.length > 1) {
            location.href = Object.query + Phrase;
        }
    });
};

$('.JS-Search-Button').JS_Search();

