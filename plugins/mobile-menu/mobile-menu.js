/*
 * Copyright (c) 2015
 * Routine JS - Mobile Menu
 * Version 0.1.3 pre-Alpha
 * Create 2015.12.01
 * Author Bunker Labs

 * Add attribute  'data-duration'       - to set duration
 * Add attribute  'data-menu-media'     - to set resolution when mobile menu should be activated (default: 1280)

 * Code structure:
 *
 * <nav class="JS-Mobile-Menu" data-duration='1000'>
 *     <div class="JS-Mobile-Menu-Toggle"></div>
 *     <ul class="JS-Mobile-Menu-List">
 *          <li>Link 1</li>
 *          <li>Link 2</li>
 *     </ul>
 * </nav>
 */

(function ($) {
    $(document).ready(function () {
        var Menu = '.JS-Mobile-Menu',
            List = '.JS-Mobile-Menu-List',
            $Toggle = $('.JS-Mobile-Menu-Toggle'),
            Duration = $(Menu).attr('data-duration') ? $(Menu).attr('data-duration') : 700,
            Media_Menu = $(Menu).attr('data-menu-media') ? $(Menu).attr('data-menu-media') : 1280,
            Is_Animated = false;

        $Toggle.click(function () {
            if ($(window).width() < Media_Menu && !Is_Animated) {
                var This_Menu = $(this).parents(Menu);
                Is_Animated = true;

                This_Menu.toggleClass('Active');
                $(List, This_Menu).toggleClass('Active').promise().done(function () {
                    Is_Animated = false;
                });
                return false;
            }
        });

        $('body').click(function () {
            if ($(window).width() < Media_Menu && !Is_Animated) {
                Is_Animated = true;
                $(List).removeClass('Active')
                    .promise()
                    .done(function () {
                        Is_Animated = false;
                    });
                $(Menu).removeClass('Active');
            }
        });

        $(window).resize(function () {
            Menu_Activation();
        });

        Menu_Activation();

        function Menu_Activation() {
            if ($(window).width() < Media_Menu) {
                $(List).removeClass('Active');
                $(Menu).removeClass('Active');
            } else {
                $(List).show();
            }
        }
    });
})(jQuery);