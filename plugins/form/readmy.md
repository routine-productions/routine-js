##Code structure:##
```
  <form class='JS-Form'
        data-form-email='admin@bunker-labs.com'
        data-form-subject='Order form website'
        data-form-url='path-to-your-script/data.form.php'
        data-form-method='POST'
        >
      <input type='text' name='name' data-input-title='Your first name'>
      <input class='JS-Form-Numeric JS-Form-Require' type='text' name='phone' data-input-title='Your telephone number'>
      <input class='JS-Form-Email JS-Form-Require' type='text' name='email' data-input-title='Your e-mail'>
      <textarea name='email' data-input-title='Your e-mail'></textarea>
      <button class='JS-Form-Button'></button>
      <div class="JS-Form-Result"></div>
  </form>
```
