/*
 * Copyright (c) 2015
 * Routine JS - Scroll on button
 * Version 0.1.1
 * Create 2015.12.18
 * Author Bunker Labs

 * Usage:
 *1)add structure
 *2)add attribute 'href' to the button and attribute 'id' to block


 * Code structure:
 * <a class="JS-Scroll-Button" href="#Block">Scroll to Block 1</a>
 * <div id="Block">Block</div>
 */
(function ($) {
    $(document).ready(function () {

        $('.JS-Scroll-Button').click(function () {

            var Data_Scroll     = $(this).attr('href'),
                Scroll_Position = $(Data_Scroll).offset().top,
                Scroll_Duration_String = $(this).attr('data-scroll-duration'),
                Scroll_Duration = parseInt(Scroll_Duration_String);


            $('body, html').animate({
                scrollTop: Scroll_Position
            }, Scroll_Duration);

            return false;
        });
    });
})(jQuery);
