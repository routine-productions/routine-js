/*
 * Copyright (c) 2015
 * Routine JS - Placeholder
 * Version 0.1.0
 * Create 2015.12.12
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Placeholder' - to create wrapper with label

 * Code structure:
 * <input class="JS-Placeholder" id="Input-1" data-animate="fade" placeholder="Name">
 */
(function ($) {
    $(document).ready(function () {
        var Input   = $('.JS-Placeholder'),
            Label   = 'JS-Label-Placeholder',
            Wrapper = '<div class="JS-Input-Wrapper"></div>';

        Input.each(function () {
            var Animation_Type = $(this).attr('data-animate') ? $(this).attr('data-animate') : 'none',
                Duration       = $(this).attr('data-duration') ? $(this).attr('data-duration') : 100;

            $(this).wrap(Wrapper).after('<label class="' + Label + '"' + ' for ="' + $(this).attr('id') + '">' +
                $(this).attr('placeholder') +
                '</label>');

            $(this).parent().css({
                'position': 'relative'
            });

            $('+.' + Label, this).css({
                'position': 'absolute',

                'top'    : 0,
                'left'   : 0,
                'padding': $(this).css('padding'),
                'margin' : $(this).css('margin')
            });

            $(this).focus(function () {

                var Input_Label = $('+.' + Label, this);

                switch (Animation_Type) {
                    case 'fade':
                    {
                        Input_Label.fadeOut(Duration);
                        break;
                    }
                    case 'slide':
                    {
                        Input_Label.animate({left: $(this).width(), 'opacity': '0'}, Duration).fadeOut(10);
                        break;
                    }
                    default :
                    {
                        Input_Label.css('display', 'none');
                        break;
                    }
                }
            });

            $(this).blur(function () {
                if (!$(this).val().length) {

                    var Input_Label = $('+.' + Label, this);
                    console.log(Label);
                    switch (Animation_Type) {
                        case 'fade':
                        {
                            Input_Label.fadeIn(Duration);
                            break;
                        }
                        case 'slide':
                        {
                            Input_Label.fadeIn(10).animate({left: '0', 'opacity': '1'}, Duration);
                            break;
                        }
                        default :
                        {
                            Input_Label.css('display', 'block');
                            break;
                        }
                    }
                }
            });
        });

        Input.removeAttr('placeholder');
    });
})(jQuery);


