<div class="Navbar">
    <ul class="Navbar-List">
        <li>
            <h4><a href="#">Content</a></h4>
            <ul class="Navbar-Sublist">
                <li><a href="/js/alert">Alert</a></li>
                <li><a href="/js/form">Form</a></li>
                <li><a href="/js/modal">Modal</a></li>
                <li><a href="/js/window">Window</a></li>
                <li><a href="/js/mobile-menu">Mobile Menu</a></li>
            </ul>
        </li>
        <li>
            <h4><a href="#">Animations</a></h4>
            <ul class="Navbar-Sublist">
                <li class="Active"><a href="/js/carousel">Carousel</a></li>
                <li><a href="/js/slider">Slider</a></li>
                <li><a href="/js/scroll-parallax">Scroll Parallax</a></li>
                <li><a href="/js/mouse-parallax">Mouse Parallax</a></li>
                <li><a href="/js/re-animation">Re Animations</a></li>
            </ul>
        </li>
        <li>
            <h4><a href="#">UI</a></h4>
            <ul class="Navbar-Sublist">
                <li><a href="/js/accordion">Accordion</a></li>
                <li><a href="/js/auto-select">Auto Select</a></li>
                <li><a href="/js/dropdown">Dropdown</a></li>
                <li><a href="/js/popover">Popover</a></li>
                <li><a href="/js/tooltop">Tooltip</a></li>
                <li><a href="/js/tabs">Tabs</a></li>
            </ul>
        </li>
        <li>
            <h4><a href="#">Plugins</a></h4>
            <ul class="Navbar-Sublist">
                <li><a href="/js/countdown">Countdown</a></li>
                <li><a href="/js/current-page">Current Page</a></li>
                <li><a href="/js/current-link">Current Link</a></li>
                <li><a href="/js/map-descroll">Map Descroll</a></li>
                <li><a href="/js/page-preloader">Page Preloader</a></li>
                <li><a href="/js/image-align">Image Align</a></li>
                <li><a href="/js/page-navigation">Page Navigation</a></li>
                <li><a href="/js/scroll-on-button">Scroll On Button</a></li>
                <li><a href="/js/scroll-top">Scroll Top</a></li>
                <li><a href="/js/typography">Typography</a></li>
            </ul>
        </li>
    </ul>
</div>