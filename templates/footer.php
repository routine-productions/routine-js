<section class="Footer">
    <div class="Footer-Wrapper">
        <ul class="Footer-Menu">
            <li class="Footer-Menu-Item"><a href="/html">HTML</a></li>
            <li class="Footer-Menu-Item"><a href="/scss">SCSS</a></li>
            <li class="Footer-Menu-Item Active"><a href="/js">JS</a></li>
            <li class="Footer-Menu-Item"><a href="/cms">CMS</a></li>
        </ul>
        <p class="Footer-Slogan">
            Organize your website according to a <em>Routine.Framework</em>.
        </p>
        <p class="Footer-Copyright">
            © Created 2016, by <a href="http://bunker-lab.com">Bunker Lab</a>
        </p>
    </div>
</section>