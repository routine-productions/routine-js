<section class="Header">
    <div class="Header-Wrapper">
        <ul class="Header-Menu">
            <li class="Header-Menu-Item"><a href="/html">HTML</a></li>
            <li class="Header-Menu-Item"><a href="/scss">SCSS</a></li>
            <li class="Header-Menu-Item Active"><a href="/js">JS</a></li>
            <li class="Header-Menu-Item"><a href="/cms">CMS</a></li>
        </ul>
        <div class="Header-Logo">
            <span class="Header-Logo-Image">
                <?php require __DIR__ . '/../img/routine.svg' ?>
            </span>
            <span class="Header-Logo-Text">ROUTINE.FRAMEWORK</span>
        </div>
    </div>
</section>